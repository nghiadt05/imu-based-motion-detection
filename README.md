# README #

This is a small project using an ADXL accelerometer sensor to detect hand movement directions. The program is written using Arduino IDE

The demo video is posted here: [demo link](https://www.youtube.com/watch?v=pQYdhvK5rA8)