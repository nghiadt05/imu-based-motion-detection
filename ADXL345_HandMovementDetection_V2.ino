/********************************************************************************
* ADXL345 Library Examples- pitch_roll.ino                                      *
*                                                                               *
* Copyright (C) 2012 Anil Motilal Mahtani Mirchandani(anil.mmm@gmail.com)       *
*                                                                               *
* License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html> *
* This is free software: you are free to change and redistribute it.            *
* There is NO WARRANTY, to the extent permitted by law.                         *
*                                                                               *
*********************************************************************************/

#include <Wire.h>
#include <ADXL345.h>

#define PRINT_MODE 4 // 0: nothing 
                     // 1: only LEFT/RIGHT
                     // 2: only FORWARD/BACKWARD
                     // 3: only LEFT/RIGHT and FORWAR/BACKWARD 
                     // 4: only circulate movements
                     // 5: all

#define HistoryAccValueSize     50
#define HistoryStaticStateSize  5
#define WaitAfterUpdatingCnt    20
#define MaximumInterval         100

#define Move_Left_Gain          -0.5
#define Move_Right_Gain          0.5
#define Move_Forward_Gain       -0.5
#define Move_Backward_Gain       0.5

const float alpha = 0.5;
const float static_gain = 0.05; // g


double fXg = 0;
double fYg = 0;
double fZg = 0;

double X_0G = 0.0f;
double Y_0G = 0.0f;

bool isStatic = false;

double HistoryAccValue[50][3];
bool   HistoryStaticState[10];

ADXL345 acc;

void setup()
{
  acc.begin();
  Serial.begin(115200);
  delay(100);
}


void loop()
{
  double pitch, roll, Xg, Yg, Zg;
  acc.read(&Xg, &Yg, &Zg);

  //Low Pass Filter
  fXg = Xg * alpha + (fXg * (1.0 - alpha));
  fYg = Yg * alpha + (fYg * (1.0 - alpha));
  fZg = Zg * alpha + (fZg * (1.0 - alpha));
    
  // update history values
  for (int i = (HistoryAccValueSize - 1); i > 0; i--)
  {
    HistoryAccValue[i][0] = HistoryAccValue[i - 1][0]; // update ACC_X history value
    HistoryAccValue[i][1] = HistoryAccValue[i - 1][1]; // update ACC_Y history value
    HistoryAccValue[i][2] = HistoryAccValue[i - 1][2]; // update ACC_Z history value
  }

  HistoryAccValue[0][0] = fXg;
  HistoryAccValue[0][1] = fYg;
  HistoryAccValue[0][2] = fZg;
  // end of update history values

  // -------------------------    check static ---------------------------------
  // if all history values of an axis have the quite same value, we assume that
  // the sensor board is static
  bool isStatic = true;  
  for ( int i = 1; i < HistoryAccValueSize; i++)
  {
    if (
      abs(fXg - HistoryAccValue[i][0]) >= static_gain ||
      abs(fYg - HistoryAccValue[i][1]) >= static_gain ||
      abs(fZg - HistoryAccValue[i][2]) >= static_gain
    )
    {      
      isStatic = false;
    }
  }  
  //Serial.println(isStatic);
  //---------------------- end of static cheking ------------------------------

  // --------------------- update offset accelerometer data -------------------
  // update Y_0G and X_1G value when the board is static and
  // always try to make the sensor board in this direction
  //
  //            /\ x
  //            |
  //            |
  //  y<-------(+)z
  //
  //           (.) (Ground)
  bool static isUpdateEnable = false;
  bool static isUpdated = false; // only update once
  int static iLoopCountAfterUpdate = 0; // the number of loops after updating offset value
  // update static history table
  for ( int i = (HistoryStaticStateSize-1); i > 0; i--)
  {
    HistoryStaticState[i] = HistoryStaticState[i-1];
  }
  HistoryStaticState[0] = isStatic;

  // decide to update the offset values or not
  isUpdateEnable = true;
  for(int i=0;i<HistoryStaticStateSize;i++)
  {
    if(HistoryStaticState[i]==false)
    {
      isUpdateEnable = false;
      isUpdated = false;
      break;
    }
  }

  // update the offset value when it's necessary
  if(isUpdateEnable && !isUpdated)
  {
    X_0G = fXg;
    Y_0G = fYg;
    isUpdated = true;
    iLoopCountAfterUpdate = 0;
    //Serial.println("Just update offset data");
  } 

  // increase the number of main loop after updating
  iLoopCountAfterUpdate ++;
  if(iLoopCountAfterUpdate>WaitAfterUpdatingCnt)
  {
    iLoopCountAfterUpdate = WaitAfterUpdatingCnt + 1;
  }
  
  // ---------------- end of update offset accelerometer data -----------------

  // ---------------- simple movement detection ----------------  
  // only process after WaitAfterUpdatingCnt loops since the
  // last update offset values. Owingto the fact that we need
  // the number of WaitAfterUpdatingCnt values in the acc history array to 
  // calculate the movement.
  bool static isLeft = false; 
  bool static isRight = false; 
  bool static isForward = false; 
  bool static isBackward = false;
  bool static isL2R_Circulate = false;
  bool static isR2L_Circulate = true;
  
  String Movement = "";
  float LR_value = 0.0f;
  float FB_value = 0.0f;

  // Left-right ; Forward-Backward movement calculation
  if(iLoopCountAfterUpdate>WaitAfterUpdatingCnt)
  {    
    for(int i=0;i<WaitAfterUpdatingCnt;i++)
    {
      LR_value += HistoryAccValue[i][1] - Y_0G;
      FB_value += HistoryAccValue[i][0] - X_0G;
    }
  }

  // left-right moving detection
  if(LR_value < Move_Left_Gain)
  {
    isLeft = true;
    isRight = false;
  }
  else if(LR_value > Move_Right_Gain)
  {
    isLeft = false;
    isRight = true;
  }
  else
  {
    isLeft = false;
    isRight = false;
  }

  // forward-backward moving detection
  if(FB_value < Move_Forward_Gain)
  {
    isForward = true;
    isBackward = false;
  }
  else if(FB_value > Move_Backward_Gain)
  {
    isForward = false;
    isBackward = true;
  }
  else
  {
    isForward = false;
    isBackward = false;
  }

  // Left to right circulate movement detection
  int static L2R_Circulate_State = 0;
  int static CntFromLastState_C1 = 0;
  isL2R_Circulate = false;
  
  // state and count value reset
  if(CntFromLastState_C1>MaximumInterval)
  {
    L2R_Circulate_State = 0;
    CntFromLastState_C1 = 0;   
  }

  // change state
  if(isLeft && isBackward)
  {
    L2R_Circulate_State = 1;
    CntFromLastState_C1 = 0;   
  }    
  
  if(L2R_Circulate_State == 1)
  {
    if( CntFromLastState_C1 < MaximumInterval && isBackward && isRight)
    {
      L2R_Circulate_State = 2;
      CntFromLastState_C1 = 0;      
    }
  }
  else if(L2R_Circulate_State == 2)
  {
    if( CntFromLastState_C1 < MaximumInterval && isForward && isRight)
    {
      L2R_Circulate_State = 3;
      CntFromLastState_C1 = 0;      
    }
  }
  else if(L2R_Circulate_State == 3)
  {
    if( CntFromLastState_C1 < MaximumInterval && isForward && isLeft)
    {
      L2R_Circulate_State = 0;
      CntFromLastState_C1 = 0;
      isL2R_Circulate = true;
    }
  }
  CntFromLastState_C1 ++;
  // end of left to right circulate movement detection

  // right to left circulate movement detection
  int static R2L_Circulate_State = 0;
  int static CntFromLastState_C2 = 0;
  isR2L_Circulate = false;
  
  // state and count value reset
  if(CntFromLastState_C2>MaximumInterval)
  {
    R2L_Circulate_State = 0;
    CntFromLastState_C2 = 0;    
  }

  // change state
  if(isRight && isBackward)
  {
    R2L_Circulate_State = 1;
    CntFromLastState_C2 = 0;
  }    
  
  if(R2L_Circulate_State == 1)
  {
    if( CntFromLastState_C2 < MaximumInterval && isBackward && isLeft)
    {
      R2L_Circulate_State = 2;
      CntFromLastState_C2 = 0;
    }
  }
  else if(R2L_Circulate_State == 2)
  {
    if( CntFromLastState_C2 < MaximumInterval && isForward && isLeft)
    {
      R2L_Circulate_State = 3;
      CntFromLastState_C2 = 0;
    }
  }
  else if(R2L_Circulate_State == 3)
  {
    if( CntFromLastState_C2 < MaximumInterval && isForward && isRight)
    {
      R2L_Circulate_State = 0;
      CntFromLastState_C2 = 0;
      isR2L_Circulate = true;
    }
  }
  CntFromLastState_C2 ++;
  // end of right to left circulate movement detection

  // Print out movement detection
  if(isL2R_Circulate)
  {
    #if PRINT_MODE == 4 || PRINT_MODE ==5
      Movement = "Left to Right circulate movement";
    #endif
  }  
  else if(isR2L_Circulate)
  {
    #if PRINT_MODE == 4 || PRINT_MODE ==5
      Movement = "Right to Left circulate movement";
    #endif
  }
  else
  {
    #if PRINT_MODE == 1 || PRINT_MODE ==3 || PRINT_MODE ==5
      if(isLeft)
      {
        Movement +="LEFT ======== ";
      }
      
      if(isRight)
      {
        Movement +="RIGHT ======= ";
      }
    #endif

    #if PRINT_MODE == 2 || PRINT_MODE ==3 || PRINT_MODE ==5
      if(isForward)
      {
        Movement +="FORWARD";
      }
  
      if(isBackward)
      {
        Movement +="BACKWARD";
      }   
    #endif
  }
  // end of print out movement detection
  
  //Serial.println(LR_value);
  //Serial.println(FB_value);
  #if PRINT_MODE != 0
    if(Movement!=NULL)
    {
      Serial.println(Movement);
    }
  #endif
    
  // -----------------------------------------------------------

  delay(5);
}






